import { DivFormulario, DivInterna } from './formulario.styles';

const Formulario = () => {
  return (
    <DivFormulario>
      <DivInterna>a</DivInterna>
    </DivFormulario>
  );
};

export default Formulario;
