import styled from 'styled-components';

export const DivItens = styled.div`
  display: flex;
  align-items: center;
  color: ${({ theme }) => theme.coresExtras.branco};

  font-size: 1.5em;
  margin: 8px 0px;
`;
