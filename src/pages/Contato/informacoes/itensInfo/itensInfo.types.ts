import React from 'react';

export interface ItensInfoProps {
  icone: React.ReactElement;
  texto: string;
  link: string;
}
