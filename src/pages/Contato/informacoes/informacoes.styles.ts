import styled from 'styled-components';

export const DivInformacoes = styled.div`
  width: 50%;
  display: flex;
  padding: 32px;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: calc(100vh - 241px);
`;
